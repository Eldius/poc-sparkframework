package net.eldiosantos.poc.spark.runcommand;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Created by Eldius on 01/10/2016.
 */
public class MainAppClass {
    public static void main(String[] params) {
        // USANDO JAVA 8
        Spark.get("/8", (req, res) -> {
            final String command = getCommand();
            try (final BufferedReader reader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec(command).getInputStream()))) {
                return reader.lines().collect(Collectors.joining("\n"));
            }
        });
        // USANDO JAVA 7
        Spark.get("/7", new Route() {
            @Override
            public Object handle(Request request, Response response) throws Exception {
                final String command = getCommand();
                final Scanner sc = new Scanner(Runtime.getRuntime().exec(command).getInputStream());
                final StringBuffer sb = new StringBuffer();
                while (sc.hasNextLine()) {
                    sb.append(sc.nextLine())
                            .append("\n");
                }
                return sb.toString();
            }
        });
    }

    private static String getCommand() {
        // Linux
        // return "ping 8.8.8.8";

        // Windows
        return "cmd /C ping 8.8.8.8";
    }
}
